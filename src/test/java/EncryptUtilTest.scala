import java.io.{FileInputStream, FileOutputStream}
import javax.crypto.KeyGenerator

import com.rizvn.dbdiff.EncryptUtil
import org.apache.commons.io.IOUtils
import org.junit.{Assert, Test}

/**
  * Created by Riz
  */
class EncryptUtilTest {

  //@Test
  def makeKey(): Unit ={
     EncryptUtil.makeKey("src/main/resources" + EncryptUtil.keyResource)
  }

  @Test
  def loadKeyTest(): Unit ={
    val key = EncryptUtil.key
    Assert.assertNotNull(key)
  }

  @Test
  def testEncrypt(): Unit ={
    EncryptUtil.encrypt("abc123", "src/test/resources/license.txt")
    val in  = IOUtils.toString(new FileInputStream("src/test/resources/license.txt"))
    val result = EncryptUtil.decrypt(in)
    Assert.assertEquals("abc123", result)
  }

  @Test
  def testGenerateLicence: Unit ={
    EncryptUtil.generateLicense(1, "license.key")
    val result = EncryptUtil.verifyLicense("license.key")
    Assert.assertTrue(result)
  }
}
