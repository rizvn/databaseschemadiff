package com.rizvn.dbdiff

import java.io.{FileOutputStream, FileInputStream}
import java.nio.charset.StandardCharsets
import java.util.{Calendar, Date}
import javax.crypto.spec.SecretKeySpec
import javax.crypto.{KeyGenerator, SecretKey, Cipher}
import javax.xml.bind.DatatypeConverter

import com.google.gson.{JsonObject, JsonParser}
import org.apache.commons.io.IOUtils

/**
  * Created by Riz
  */
object EncryptUtil {
  val keyResource = "/dsk"

  lazy val key : SecretKey =  {
    val is = getClass.getResourceAsStream(keyResource)
    new SecretKeySpec(IOUtils.toByteArray(is), "AES")
  }

  def makeKey(keyPath: String): Unit = {
    val generator = KeyGenerator.getInstance("AES")
    generator.init(128)
    val key = generator.generateKey()
    IOUtils.write(key.getEncoded, new FileOutputStream(keyPath))
  }

  def encrypt(input: String, outputPath: String) = {
    val cipher = Cipher.getInstance("AES")
    cipher.init(Cipher.ENCRYPT_MODE, key)

    val encrypted = cipher.doFinal(input.getBytes(StandardCharsets.UTF_8))
    val base64encryped = DatatypeConverter.printBase64Binary(encrypted)
    val os = new FileOutputStream(outputPath)
    IOUtils.write(base64encryped, os)
  }

  def decrypt(input: String):  String = {
    val cipher = Cipher.getInstance("AES")
    cipher.init(Cipher.DECRYPT_MODE, key)

    val bytes = DatatypeConverter.parseBase64Binary(input)
    val decrypted = cipher.doFinal(bytes)

    new String(decrypted, StandardCharsets.UTF_8)
  }


  def generateLicense(expireMonths: Int, outputPath: String): Unit ={
    val json = new JsonObject
    val calendar = Calendar.getInstance()
    calendar.add(Calendar.MONTH, expireMonths)
    json.addProperty("expires", calendar.getTimeInMillis)
    val licenseString = json.toString
    encrypt(licenseString, outputPath)
  }

  def verifyLicense(licensePath: String): Boolean = {
    val encrypted = IOUtils.toString(new FileInputStream(licensePath), StandardCharsets.UTF_8.name())
    val contents = EncryptUtil.decrypt(encrypted)
    val json = new JsonParser().parse(contents).getAsJsonObject

    val expiretimestamp = json.getAsJsonPrimitive("expires").getAsLong
    expiretimestamp > Calendar.getInstance().getTimeInMillis
  }
}
