package com.rizvn.dbdiff

import java.sql.{Driver, DatabaseMetaData, Connection}
import java.util.Properties

/**
  * Created by Riz
  */
class Db {
  var id: String = null
  var user: String = null
  var pwd: String = null
  var url: String = null
  var driverName: String = null
  var conn: Connection = null
  var metadata: DatabaseMetaData = null

  def getConnection: Connection = {
    if (conn == null) {
        val driver: Driver = Class.forName(driverName).newInstance.asInstanceOf[Driver]
        val props: Properties = new Properties
        props.setProperty("user", user)
        props.setProperty("password", pwd)
        conn = driver.connect(url, props)
    }
    conn
  }

  def getMetaData: DatabaseMetaData = {
    if (metadata == null) {
      metadata = getConnection.getMetaData
    }
    metadata
  }
}
