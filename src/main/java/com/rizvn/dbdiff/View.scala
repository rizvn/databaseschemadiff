package com.rizvn.dbdiff

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{GridBagConstraints, GridBagLayout, BorderLayout, GridLayout}
import java.io.FileInputStream
import java.nio.charset.StandardCharsets
import java.util.concurrent.{Executors, ThreadPoolExecutor}
import javax.swing._
import javax.swing.border.EmptyBorder

import com.google.gson.{JsonParser, JsonObject}
import org.apache.commons.io.IOUtils

/**
  * Created by Riz
  */
class View extends ActionListener{
  var evaluation   = true
  var compareCount = 0
  val gridBadConstraint = new GridBagConstraints()

  val lblDb1Url    = new JLabel("DB1 url: ")
  val lblDb1User   = new JLabel("DB1 user: ")
  val lblDb1Driver = new JLabel("DB1 driver: ")
  val lblDb1Pass   = new JLabel("DB1 pass: ")

  val lblDb2Url    = new JLabel("DB2 url: ")
  val lblDb2User   = new JLabel("DB2 user: ")
  val lblDb2Pass   = new JLabel("DB2 pass: ")
  val lblDb2Driver = new JLabel("DB2 driver: ")

  val txtDb1Url    = new JTextField(250)
  val txtDb1User   = new JTextField(30)
  val txtDb1Pass   = new JTextField(50)
  val txtDb2Url    = new JTextField(250)
  val txtDb2User   = new JTextField(30)
  val txtDb2Pass   = new JTextField(30)

  val cmbDb1Driver  = new JComboBox[String]
  val cmbDb2Driver  = new JComboBox[String]
  val cmbComparison = new JComboBox[String]
  val controller = new Controller(this)

  var txtOutput = new JTextPane
  var butCompare = new JButton("Compare")

  val executor = {
    val ex = Executors.newFixedThreadPool(50)
    ex.asInstanceOf[ThreadPoolExecutor]
  }


  def constraint(x:Int, y: Int, spanx: Int = 1, spany: Int = 1): GridBagConstraints ={
    gridBadConstraint.gridx = x
    gridBadConstraint.gridy = y
    gridBadConstraint.gridwidth = spanx
    gridBadConstraint.gridheight = spany
    gridBadConstraint.fill = GridBagConstraints.HORIZONTAL
    return gridBadConstraint
  }

  def init(){
    var title = "DB Schema DIff"

    if(evaluation) title += " [EVALUATION]"

    val frame = new JFrame(title)
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)

    val contentPane = frame.getContentPane()
    contentPane.setLayout(new BorderLayout())

    val optionsCtr1 = new JPanel(new BorderLayout())

    val optionsCtr2 = new JPanel(new GridLayout(0,2))

    optionsCtr2.setBorder(new EmptyBorder(10, 10, 10, 10))
    val leftOptions = new JPanel(new GridBagLayout())
    val labelConstraints = new GridBagConstraints()

    labelConstraints.weightx = 1
    labelConstraints.weighty = 1

    val fieldContraints = new GridBagConstraints()
    fieldContraints.weightx = 2
    fieldContraints.weighty = 1


    val rightOptions = new JPanel(new GridBagLayout())
    rightOptions.setBorder(new EmptyBorder(0, 3, 0, 0))

    optionsCtr2.add(leftOptions)
    optionsCtr2.add(rightOptions)

    leftOptions.add(lblDb1Url, constraint(0, 0))
    txtDb1Url.setBounds(0, 0, 30, 10)
    leftOptions.add(txtDb1Url, constraint(1, 0, spanx = 2))

    leftOptions.add(lblDb1User,constraint(0, 1))
    leftOptions.add(txtDb1User, constraint(1,1, spanx = 2))

    leftOptions.add(lblDb1Pass, constraint(0, 2))
    leftOptions.add(txtDb1Pass, constraint(1, 2, spanx = 2))

    leftOptions.add(lblDb1Driver, constraint(0, 3))
    leftOptions.add(cmbDb1Driver, constraint(1, 3, spanx = 2))

    rightOptions.add(lblDb2Url, constraint(0, 0))
    rightOptions.add(txtDb2Url, constraint(1, 0, spanx = 2))

    rightOptions.add(lblDb2User, constraint(0, 1))
    rightOptions.add(txtDb2User, constraint(1,1, spanx = 2))

    rightOptions.add(lblDb2Pass, constraint(0, 2))
    rightOptions.add(txtDb2Pass, constraint(1, 2, spanx = 2))

    rightOptions.add(lblDb2Driver, constraint(0, 3))
    rightOptions.add(cmbDb2Driver, constraint(1, 3, spanx = 2))

    cmbComparison.addItem("DB1 with DB2")
    cmbComparison.addItem("DB2 with DB1")

    val optionsCtr3 = new JPanel()
    optionsCtr3.add(new JLabel("Compare:"))
    optionsCtr3.add(cmbComparison)
    optionsCtr3.add(butCompare)

    populateDrivers(cmbDb1Driver)
    populateDrivers(cmbDb2Driver)

    optionsCtr1.add(optionsCtr2, BorderLayout.NORTH)
    optionsCtr1.add(optionsCtr3, BorderLayout.SOUTH)

    contentPane.add(optionsCtr1, BorderLayout.NORTH)
    val scroller = new JScrollPane(txtOutput)
    contentPane.add(scroller, BorderLayout.CENTER)

    frame.setSize(600, 600)
    frame.setVisible(true)

    butCompare.addActionListener(this)
    butCompare.setActionCommand(controller.CMD_COMPARE)
  }

  override def actionPerformed(e: ActionEvent): Unit = {
    executor.submit(new Runnable { override def run(): Unit = controller.dispatch(e) })
  }

  def populateDrivers(aCombobox: JComboBox[String]) {
    aCombobox.addItem("com.mysql.jdbc.Driver")

    if (!evaluation) {
      aCombobox.addItem("com.microsoft.sqlserver.jdbc.SQLServerDriver")
      aCombobox.addItem("oracle.jdbc.OracleDriver")
      aCombobox.addItem("org.h2.Driver")
    }
  }

  def checkLicense(): Boolean ={
    EncryptUtil.verifyLicense("license.key")
  }

  def parseJson(input: String) : JsonObject = {
    val jsonParser = new JsonParser()
    val jsonEl = jsonParser.parse(input)
    jsonEl.getAsJsonObject
  }
}


object View{
  def main(args: Array[String]): Unit ={
    val gui = View()
  }

  //factory method to create view
  def apply() : View = {
    val view  = new View
    view.init()
    view
  }
}
