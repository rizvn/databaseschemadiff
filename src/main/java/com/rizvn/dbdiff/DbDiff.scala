package com.rizvn.dbdiff

import java.sql.{DatabaseMetaData, ResultSet}
import java.util.{HashMap, Map, SortedSet, TreeSet}
import javax.swing.JTextPane
import scala.collection.JavaConversions._

/**
  * Created by Riz
  */
class DbDiff {
  var txtOutput: JTextPane = null

  def outputMessage(aMessage: String) {
    if (txtOutput != null) {
      txtOutput.setText(txtOutput.getText + "\n" + aMessage)
    }
    else {
      println(aMessage)
    }
  }

  def compare(aDb1: Db, aDb2: Db) {
    var differencesFound = 0
    outputMessage("----------------------------------------")
    outputMessage(String.format("Comparing %s with %s", aDb1.id, aDb2.id))
    outputMessage("----------------------------------------")

    //get metadata for the 2 tables
    val db1Tables = getTableNames(aDb1.getMetaData)
    val db2Tables = getTableNames(aDb2.getMetaData)

    for (db1Table <- db1Tables) {
      if (db2Tables.contains(db1Table)) {
        val db1TableCols = getColumns(aDb1.getMetaData, db1Table)
        val db2TableCols = getColumns(aDb2.getMetaData, db1Table)

        for (column <- db1TableCols.keySet) {
          if (!db2TableCols.containsKey(column)) {
            differencesFound += 1
            outputMessage(String.format("Missing column:\t %s.%s.%s", aDb2.id, db1Table, column))
          }
          else {
            val column1: Column = db1TableCols.get(column)
            val column2: Column = db2TableCols.get(column)
            if (!(column1.nullable == column2.nullable)) {
              differencesFound += 1
              outputMessage(String.format("Null mismatch:\t %s.%s.%s [%s: %s, %s: %s]", aDb2.id, db1Table, column, aDb1.id, column1.nullable, aDb2.id, column2.nullable))
            }
            if (!(column1.length == column2.length)) {
              differencesFound += 1
              outputMessage(String.format("Length mismatch:\t %s.%s.%s [%s: %s, %s: %s]", aDb2.id, db1Table, column, aDb1.id, column1.length, aDb2.id, column2.length))
            }
            if (!(column1.colType == column2.colType)) {
              differencesFound += 1
              outputMessage(String.format("Type mismatch:\t %s.%s.%s [%s: %s, %s: %s]", aDb2.id, db1Table, column, aDb1.id, column1.colType, aDb2.id, column2.colType))
            }
            if (!(column1.digits == column2.digits)) {
              differencesFound += 1
              outputMessage(String.format("Digits mismatch:\t %s.%s.%s [%s: %s, %s: %s]", aDb2.id, db1Table, column, aDb1.id, column1.digits, aDb2.id, column2.digits))
            }
          }
        }
      }
      else {
        differencesFound += 1
        outputMessage(String.format("Missing table:\t %s.%s", aDb2.id, db1Table))
      }
    }
    outputMessage(String.format("\n%s differences found \n", differencesFound + ""))
  }

  /**
    * Get Table names for a database connection
    * @param aDbMd Database meta data
    * @return Column names sorted alphabetically
    */
  def getTableNames(aDbMd: DatabaseMetaData): SortedSet[String] = {
    val tableNames = new TreeSet[String]
    val rs = aDbMd.getTables(null, null, "%", Array[String]("TABLE"))
    while (rs.next) {
      tableNames.add(rs.getString("TABLE_NAME"))
    }
    return tableNames
  }


  /**
    * Get column meta data for a table
    * @param aDbMd Database metadata
    * @param aTable Table name for which to get colums
    * @return Columns
    */
  def getColumns(aDbMd: DatabaseMetaData, aTable: String): Map[String, Column] = {
    val columns = new HashMap[String, Column]
    val rs = aDbMd.getColumns(null, null, aTable, "%")
    while (rs.next) {
      val column: Column = new Column()
      column.name = rs.getString("COLUMN_NAME")
      column.colType = rs.getString("TYPE_NAME")
      column.nullable = rs.getString("IS_NULLABLE")
      column.length = rs.getString("COLUMN_SIZE")
      column.digits = rs.getString("DECIMAL_DIGITS")
      column.digits = if (column.digits == null) "" else column.digits
      columns.put(column.name, column)
    }
    return columns
  }

}

class Column {
  var name: String = null
  var colType: String = null
  var nullable: String = null
  var length: String = null
  var digits: String = null
}
