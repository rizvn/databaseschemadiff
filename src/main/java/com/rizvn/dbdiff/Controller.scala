package com.rizvn.dbdiff

import java.awt.Color
import java.awt.event.ActionEvent
import javax.swing.{BorderFactory, JTextField}

import org.apache.commons.lang.exception.ExceptionUtils
import org.apache.commons.lang3.StringUtils

/**
  * Created by Riz
  */
class Controller(view: View){
  val CMD_COMPARE = "CMD_COMPARE"

  //function does case match events action command
  def dispatch(e: ActionEvent): Unit = e.getActionCommand match{
      case CMD_COMPARE => compare()
  }

  def compare(): Unit ={
    if(view.evaluation){
      view.compareCount += 1
      if(view.compareCount > 2){
        view.txtOutput.setText("Maximum comparisons reached for evaluation version")
      }
    }

    //if invalid
    if(!validate()) return


    view.butCompare.setEnabled(false)
    view.butCompare.setText("comparing...")

    view.txtOutput.setText("")
    val diff: DbDiff = new DbDiff
    diff.txtOutput = view.txtOutput

    try {
      val db1: Db = new Db
      db1.id = "DB1"
      db1.url = view.txtDb1Url.getText
      db1.user = view.txtDb1User.getText
      db1.pwd = view.txtDb1Pass.getText
      db1.driverName = view.cmbDb1Driver.getSelectedItem.toString

      val db2: Db = new Db
      db2.id = "DB2"
      db2.url = view.txtDb2Url.getText
      db2.user = view.txtDb2User.getText
      db2.pwd = view.txtDb2Pass.getText
      db2.driverName = view.cmbDb2Driver.getSelectedItem.toString

      if (view.cmbComparison.getSelectedIndex == 0) {
        diff.compare(db1, db2)
      }
      else {
        diff.compare(db2, db1)
      }
    }
    catch{
      case exception: Exception => {
        val msg = ExceptionUtils.getStackTrace(exception)
        diff.outputMessage(msg)
      }
    }
    finally {
      view.butCompare.setEnabled(true)
      view.butCompare.setText("compare")
    }
  }

  def validate(): Boolean ={
    var inValid = false

    inValid = checkIsBlank(view.txtDb1Url)  || inValid
    inValid = checkIsBlank(view.txtDb1User) || inValid
    inValid = checkIsBlank(view.txtDb1Pass) || inValid

    inValid = checkIsBlank(view.txtDb2Url)  || inValid
    inValid = checkIsBlank(view.txtDb2User) || inValid
    inValid = checkIsBlank(view.txtDb2Pass) || inValid

    !inValid
  }

  def checkIsBlank(textField: JTextField): Boolean ={
    var result = false
    var normalBorder = new JTextField().getBorder
    if(StringUtils.isBlank(textField.getText)){
      val border = BorderFactory.createLineBorder(Color.RED, 2)
      textField.setBorder(border)
      result = true
    }
    else {
      textField.setBorder(normalBorder)
    }
    result
  }

}
